package ihm.sophiares.event;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * A fake event of Cap Sophia.
 *
 * @author Guillaume André
 */
public class EventSophia {

    private String title;
    private String author;
    private String details;
    private List<String> tags;
    private long date;

    /**
     * Constructor.
     *
     * @param title   the title of the event
     * @param author  the author of the event
     * @param details the summary of the event
     * @param date    the date of the event (Unix timestamp in ms)
     * @param tags    the tag list of the event
     */
    public EventSophia(String title, String author, String details, long date, List<String> tags) {
        this.title = title;
        this.author = author;
        this.date = date;
        this.details = details;
        this.tags = tags;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getDetails() {
        return details;
    }

    /**
     * Formats the date to a user-friendly format before returning it.
     *
     * @return a date readable by humans
     */
    public String getDate() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        return format.format(new Date(date));
    }

    /**
     * Formats the list to an user-friendly format before displaying it.
     *
     * @return the tags as String
     */
    public String getTags() {
        return formatTags(tags);
    }

    /**
     * Removes the brackets from the String representation of the tags.
     *
     * @param tags the tag list of the event
     * @return a String representation of the list, without the brackets
     */
    private String formatTags(List<String> tags) {
        return tags.toString().substring(1, tags.toString().length() - 1);
    }

}
