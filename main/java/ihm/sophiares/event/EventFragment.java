package ihm.sophiares.event;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ihm.sophiares.R;

/**
 * Fragment used to display a list of events.
 *
 * @author Guillaume André
 */
public class EventFragment extends Fragment {

    private View root;
    private List<EventSophia> content;

    /**
     * Sets the events to display in this fragment.
     *
     * @param content - the list of events to display
     */
    public void setContent(List<EventSophia> content) {
        this.content = content;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.event_list, container, false);
        if (content.isEmpty()) {
            inflater.inflate(R.layout.placeholder, (ViewGroup) root);
        }
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.recycler);
        EventRecyclerAdapter adapter = new EventRecyclerAdapter(content, getActivity());
        RecyclerView.LayoutManager manager = new GridLayoutManager(getActivity().getApplicationContext(), 2);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }

}
