package ihm.sophiares.event;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import ihm.sophiares.R;

/**
 * @author Guillaume André
 */
public class EventViewerFragment extends DialogFragment {

    private EventSophia event;

    public void init(EventSophia event) {
        this.event = event;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View root = inflater.inflate(R.layout.event, null);
        ((TextView) root.findViewById(R.id.author)).setText(event.getAuthor());
        ((TextView) root.findViewById(R.id.date)).setText(event.getDate());
        ((TextView) root.findViewById(R.id.summary)).setText(event.getTitle());
        ((TextView) root.findViewById(R.id.content)).setText(event.getDetails());
        ((TextView) root.findViewById(R.id.tags)).setText(event.getTags());
        builder.setView(root);
        return builder.create();
    }

}
