package ihm.sophiares.event;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ihm.sophiares.R;

/**
 * View holder for a Cap Sophia Event.
 *
 * @author Guillaume André
 */
public class EventViewHolder extends RecyclerView.ViewHolder {

    private Activity activity;

    /**
     * Constructor.
     *
     * @param itemView the main view of the event
     */
    public EventViewHolder(View itemView, Activity activity) {
        super(itemView);
        this.activity = activity;
    }

    /**
     * Binds this view to a particular event.
     *
     * @param event the event to bind this view to
     */
    public void bindToNotification(EventSophia event) {
        itemView.setOnClickListener((x) -> {
            EventViewerFragment fragment = new EventViewerFragment();
            fragment.init(event);
            fragment.show(activity.getFragmentManager(), "");
        });
        ((TextView) itemView.findViewById(R.id.author)).setText(event.getAuthor());
        ((TextView) itemView.findViewById(R.id.date)).setText(event.getDate());
        ((TextView) itemView.findViewById(R.id.summary)).setText(event.getTitle());
        String details = event.getDetails();
        if (details.length() > 130) {
            details = details.substring(0, 130) + "...";
        }
        ((TextView) itemView.findViewById(R.id.content)).setText(details);
        ((TextView) itemView.findViewById(R.id.tags)).setText(event.getTags());
    }


}
