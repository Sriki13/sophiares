package ihm.sophiares.event;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ihm.sophiares.R;

/**
 * Adapter for Cap Sophia events.
 *
 * @author Guillaume André
 */
public class EventRecyclerAdapter extends RecyclerView.Adapter<EventViewHolder> {

    private List<EventSophia> notifications;
    private Activity activity;

    /**
     * Constructor.
     *
     * @param notifications the list of events to display
     */
    public EventRecyclerAdapter(List<EventSophia> notifications, Activity activity) {
        this.notifications = notifications;
        this.activity = activity;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflated = LayoutInflater.from(parent.getContext()).inflate(R.layout.event, parent, false);
        return new EventViewHolder(inflated, activity);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        holder.bindToNotification(notifications.get(position));
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

}
