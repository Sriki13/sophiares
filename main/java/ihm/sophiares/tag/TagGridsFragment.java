package ihm.sophiares.tag;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ihm.sophiares.R;
import ihm.sophiares.config.TagGridListener;

/**
 * Fragment used to manage the grids used to select tags.
 *
 * @author Guillaume André
 */
public class TagGridsFragment extends Fragment implements TagPickerListener {

    private GridLayout includeGrid;
    private GridLayout excludeGrid;
    private TagGridListener listener;

    /**
     * Sets th listener to cal when the gird are inflated for further actions.
     *
     * @param listener the listener to call after onCreateView()
     */
    public void setListener(TagGridListener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.tag_grids, container, false);
        includeGrid = (GridLayout) root.findViewById(R.id.include);
        includeGrid.setOnClickListener((x) -> newTag(true));
        excludeGrid = (GridLayout) root.findViewById(R.id.exclude);
        excludeGrid.setOnClickListener((x) -> newTag(false));
        loadEmptyGrid(includeGrid);
        loadEmptyGrid(excludeGrid);
        if (listener != null) {
            listener.notifyLayoutDrawn();
        }
        return root;
    }

    /**
     * Loads the placeholder element for an empty tag grid: an edit button and a TextView
     * describing the default behavior.
     *
     * @param grid the grid layout to fill
     */
    private void loadEmptyGrid(GridLayout grid) {
        boolean include = grid.getId() == R.id.include;
        LayoutInflater inflater = getActivity().getLayoutInflater();
        ImageView image = (ImageView) inflater.inflate(R.layout.edit_button, null);
        image.setOnClickListener((x) -> newTag(include));
        grid.addView(image);
        View text = inflater.inflate(R.layout.tag, null);
        TextView tag = (TextView) text.findViewById(R.id.tagText);
        tag.setText(include ? "Tous" : "Aucun");
        grid.addView(tag);
    }

    /**
     * Opens a dialog to select the tags to include in one of the grids.
     *
     * @param include true for the whitelist grid, false for the other one
     */
    private void newTag(boolean include) {
        TagPickerFragment fragment = new TagPickerFragment();
        fragment.init(include, this, fetchTags(include));
        fragment.show(getFragmentManager(), "");
    }

    /**
     * Changes the tags of a grid to the ones selected in a tag picker dialog.
     *
     * @param selected  the list of tags selected
     * @param isInclude true if loading into the whitelist grid, false for the other one
     */
    @Override
    public void changeTags(List<String> selected, boolean isInclude) {
        GridLayout grid = isInclude ? includeGrid : excludeGrid;
        grid.removeAllViews();
        if (selected.isEmpty()) {
            loadEmptyGrid(grid);
            return;
        }
        for (String tag : selected) {
            View view = LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.tag, null);
            TextView text = (TextView) view.findViewById(R.id.tagText);
            text.setText(tag);
            grid.addView(view);
        }
    }

    /**
     * Returns a list of currently selected tags.
     *
     * @param include true to fetch whitelisting tags, false for blacklisting
     * @return a list of the tags selected in one of the grids
     */
    public List<String> fetchTags(boolean include) {
        GridLayout grid = include ? includeGrid : excludeGrid;
        List<String> result = new ArrayList<>();
        if (grid.getChildAt(0).getId() != R.id.editButton) {
            for (int i = 0; i < grid.getChildCount(); i++) {
                TextView text = (TextView) grid.getChildAt(i);
                result.add(text.getText().toString());
            }
        }
        return result;
    }

    /**
     * Loads tags into the grids.
     *
     * @param whitelist the list of tags to load in the whitelist grid
     * @param blacklist the list of tags to load in the blacklist grid
     */
    public void loadIntoGrids(List<String> whitelist, List<String> blacklist) {
        loadTagsInGrid(includeGrid, whitelist);
        loadTagsInGrid(excludeGrid, blacklist);
    }

    /**
     * Loads a list of tags into a grid.
     *
     * @param grid the grid layout to fill
     * @param tags the list of tags to insert
     */
    private void loadTagsInGrid(GridLayout grid, List<String> tags) {
        grid.removeAllViews();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        for (String tag : tags) {
            TextView text = (TextView) inflater.inflate(R.layout.tag, null);
            text.setText(tag);
            grid.addView(text);
        }
    }

}
