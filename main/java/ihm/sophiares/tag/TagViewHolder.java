package ihm.sophiares.tag;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * View holder a single tag checkbox in the tag picker.
 *
 * @author Guillaume André
 */
public class TagViewHolder extends RecyclerView.ViewHolder {

    /**
     * Constructor.
     *
     * @param itemView the tag main view
     */
    public TagViewHolder(View itemView) {
        super(itemView);
    }

}
