package ihm.sophiares.tag;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import java.util.List;

import ihm.sophiares.R;
import ihm.sophiares.database.SophiaDB;

/**
 * Fragment controlling the tag picker dialog.
 *
 * @author Guillaume André
 */
public class TagPickerFragment extends DialogFragment {

    private TagPickerListener listener;
    private TagAdapter adapter;
    private boolean include;
    private List<String> checkedTags;

    /**
     * Initializes the dialog content.
     *
     * @param include     whether the tags picked are for whitelisting (true) or blacklisting (false)
     * @param listener    the listener to call once the selection is confirmed
     * @param checkedTags the list of tags already selected
     */
    public void init(boolean include, TagPickerListener listener, List<String> checkedTags) {
        this.include = include;
        this.checkedTags = checkedTags;
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View root = inflater.inflate(R.layout.tag_picker, null);
        root.findViewById(R.id.confirm_tag_selection).setOnClickListener((x) -> confirmTags());
        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.tagRecycler);
        adapter = new TagAdapter(SophiaDB.getInstance(getActivity().getApplicationContext()).getAllTags(), checkedTags);
        RecyclerView.LayoutManager manager = new GridLayoutManager(getActivity().getApplicationContext(), 3);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        builder.setView(root);
        return builder.create();
    }

    /**
     * Confirms the current tag selection and closes the dialog.
     */
    private void confirmTags() {
        listener.changeTags(adapter.getSelected(), include);
        dismiss();
    }


}
