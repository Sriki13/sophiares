package ihm.sophiares.tag;

import java.util.List;

/**
 * Interface used to pick tags.
 *
 * @author Guillaume André
 */
public interface TagPickerListener {

    /**
     * Changes a list of tags to a new selection of tags.
     *
     * @param selected the list of tags selected
     * @param include  true for whitelist tags, false for blacklist tags
     */
    void changeTags(List<String> selected, boolean include);

}
