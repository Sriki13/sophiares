package ihm.sophiares.tag;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.util.ArrayList;
import java.util.List;

import ihm.sophiares.R;

/**
 * Adapter used to display the available tags.
 *
 * @author Guillaume André
 */
public class TagAdapter extends RecyclerView.Adapter<TagViewHolder> {

    private List<String> tags;
    private List<String> checkedTags;
    private List<CheckBox> items;

    /**
     * Constructor.
     *
     * @param tags        the tags to display
     * @param checkedTags the tags already selected
     */
    public TagAdapter(List<String> tags, List<String> checkedTags) {
        this.tags = tags;
        this.checkedTags = checkedTags;
        items = new ArrayList<>();
    }

    @Override
    public TagViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflated = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_checkbox, parent, false);
        items.add((CheckBox) inflated.findViewById(R.id.element_box));
        return new TagViewHolder(inflated);
    }

    @Override
    public void onBindViewHolder(TagViewHolder holder, int position) {
        items.get(position).setText(tags.get(position));
        if (checkedTags.contains(tags.get(position))) {
            items.get(position).toggle();
        }
    }

    @Override
    public int getItemCount() {
        return tags.size();
    }

    public List<String> getSelected() {
        List<String> result = new ArrayList<>();
        for (CheckBox box : items) {
            if (box.isChecked()) {
                result.add(box.getText().toString());
            }
        }
        return result;
    }

}
