package ihm.sophiares.delete;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * View holder for a single filter.
 *
 * @author Guillaume André
 */
public class FilterViewHolder extends RecyclerView.ViewHolder {

    /**
     * Constructor.
     *
     * @param itemView the filter main view
     */
    public FilterViewHolder(View itemView) {
        super(itemView);
    }

}
