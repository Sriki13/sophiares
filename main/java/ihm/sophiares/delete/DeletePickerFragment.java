package ihm.sophiares.delete;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import java.io.File;
import java.util.List;

import ihm.sophiares.MainActivity;
import ihm.sophiares.R;

/**
 * Fragment controlling the dialog used to delete filters.
 *
 * @author Guillaume André
 */
public class DeletePickerFragment extends DialogFragment {

    private FilterAdapter adapter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View root = inflater.inflate(R.layout.delete_filter, null);
        root.findViewById(R.id.confirm_deletion).setOnClickListener((x) -> deleteFilters());
        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.filter_recycler);
        adapter = new FilterAdapter(getActivity().getFilesDir());
        if (adapter.isEmpty()) {
            View placeholder = inflater.inflate(R.layout.placeholder, null);
            ((LinearLayout) root).addView(placeholder, 1);
        }
        RecyclerView.LayoutManager manager = new GridLayoutManager(getActivity().getApplicationContext(), 3);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        builder.setView(root);
        return builder.create();
    }

    /**
     * Deletes the selected filter from the app file directory.
     * When done, closes the dialog.
     */
    private void deleteFilters() {
        List<String> delete = adapter.getSelected();
        for (String name : delete) {
            File file = new File(getActivity().getFilesDir(), name + ".json");
            if (!file.delete()) {
                Log.e("FILTER_DELETION", "Failed to delete filter " + name);
            }
        }
        ((MainActivity) getActivity()).loadMenuFilters();
        dismiss();
    }


}
