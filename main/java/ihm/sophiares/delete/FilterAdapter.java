package ihm.sophiares.delete;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ihm.sophiares.R;

import static ihm.sophiares.MainActivity.removeJSON;

/**
 * Adapter for the filters saved on the device.
 *
 * @author Guillaume André
 */
public class FilterAdapter extends RecyclerView.Adapter<FilterViewHolder> {

    private List<String> filters;
    private List<CheckBox> items;

    /**
     * Constructor.
     *
     * @param directory the app main file directory
     */
    public FilterAdapter(File directory) {
        filters = new ArrayList<>();
        items = new ArrayList<>();
        for (File file : directory.listFiles()) {
            if (file.getName().endsWith(".json")) {
                filters.add(removeJSON(file.getName()));
            }
        }
    }

    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflated = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_checkbox, parent, false);
        items.add((CheckBox) inflated.findViewById(R.id.element_box));
        return new FilterViewHolder(inflated);
    }

    @Override
    public void onBindViewHolder(FilterViewHolder holder, int position) {
        items.get(position).setText(filters.get(position));
    }

    @Override
    public int getItemCount() {
        return filters.size();
    }

    /**
     * Returns a list of all the names of the filters selected for deletion.
     *
     * @return a list of filter names to be deleted
     */
    public List<String> getSelected() {
        List<String> result = new ArrayList<>();
        for (CheckBox box : items) {
            if (box.isChecked()) {
                result.add(box.getText().toString());
            }
        }
        return result;
    }

    /**
     * Checks if no saved filters were found on the device.
     *
     * @return true if no filters were found, false otherwise
     */
    public boolean isEmpty() {
        return filters.isEmpty();
    }

}
