package ihm.sophiares.database;

import static ihm.sophiares.database.DBStatements.TAGS_INSERT_BASE;

/**
 * Enum for all the fake tags for the events.
 *
 * @author Guillaume André
 */
public enum MockTag {

    OPENING(0, "Ouverture"),
    CLOSING(1, "Fermeture"),
    FLASH_SALE(2, "Promotion"),
    GOODIES(3, "Goodies"),
    CHRISTMAS(4, "Noel"),
    GAME(5, "Jeu-concours"),
    EVENT_TAG(6, "Evènement"),
    MOTHERS_DAY(7, "Fête des mères"),
    NEWS(8, "Actualité");

    private String statement;
    private int id;

    /**
     * Constructor.
     *
     * @param id   the id of the tag
     * @param name the name of the tag
     */
    MockTag(int id, String name) {
        this.id = id;
        statement = TAGS_INSERT_BASE + id + ",'" + name + "');";
    }

    public int getId() {
        return id;
    }

    public String getStatement() {
        return statement;
    }


}
