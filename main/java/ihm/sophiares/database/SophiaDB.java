package ihm.sophiares.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import ihm.sophiares.event.EventSophia;

import static ihm.sophiares.database.DBStatements.BASE_DATE;
import static ihm.sophiares.database.DBStatements.EventEntry;
import static ihm.sophiares.database.DBStatements.EventEntry.EVENT_AUTHOR;
import static ihm.sophiares.database.DBStatements.EventEntry.EVENT_DATE;
import static ihm.sophiares.database.DBStatements.EventEntry.EVENT_DETAILS;
import static ihm.sophiares.database.DBStatements.EventEntry.EVENT_TABLE;
import static ihm.sophiares.database.DBStatements.EventEntry.EVENT_TITLE;
import static ihm.sophiares.database.DBStatements.TAG_TO_NOTIFICATION;
import static ihm.sophiares.database.DBStatements.TagEntry;
import static ihm.sophiares.database.DBStatements.TagEntry.TAGS_TABLE;
import static ihm.sophiares.database.DBStatements.TagEntry.TAG_NAME;


/**
 * Helper class to manage database requests.
 *
 * @author Guillaume André
 */
public class SophiaDB extends SQLiteOpenHelper {

    private static final String SQL_CREATE_NOTIFICATIONS =
            "CREATE TABLE " + EVENT_TABLE + " (" +
                    EventEntry._ID + " INTEGER PRIMARY KEY," +
                    EVENT_TITLE + " TEXT," + EVENT_AUTHOR + " TEXT,"
                    + EVENT_DETAILS + " TEXT," + EVENT_DATE + " INTEGER);";

    private static final String SQL_CREATE_TAGS =
            "CREATE TABLE " + TAGS_TABLE + " (" +
                    TagEntry._ID + " INTEGER PRIMARY KEY," + TAG_NAME + " TEXT);";

    private static final String SQL_CREATE_TAG_TO_NOTIFICATION =
            "CREATE TABLE " + TAG_TO_NOTIFICATION + " (" +
                    TagEntry._ID + " INTEGER REFERENCES " + TAGS_TABLE + "(" + TagEntry._ID + ")," +
                    EventEntry._ID + " INTEGER REFERENCES " + EVENT_TABLE + "(" + EventEntry._ID + "));";

    private static final String SELECT_NOTIF_BASE = "SELECT * FROM " + EVENT_TABLE;
    private static SophiaDB instance;

    /**
     * Constructor.
     *
     * @param context the context of the application
     */
    private SophiaDB(Context context) {
        super(context, "SophiaResDB", null, 1);
    }

    /**
     * Ensures only one instance of the helper is called for the whole application.
     *
     * @param context - the context of the application
     * @return the instance of the helper
     */
    public static SophiaDB getInstance(Context context) {
        if (instance == null) {
            instance = new SophiaDB(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_NOTIFICATIONS);
        db.execSQL(SQL_CREATE_TAGS);
        db.execSQL(SQL_CREATE_TAG_TO_NOTIFICATION);
        mockTags(db);
        mockEvents(db);
    }

    /**
     * Executes all the tags Insert statements, filling the tag table.
     *
     * @param db the database to fill
     */
    public void mockTags(SQLiteDatabase db) {
        for (MockTag tag : MockTag.values()) {
            db.execSQL(tag.getStatement());
        }
    }

    /**
     * Executes all the Insert statements related to the events, filling the events table
     * and the tag association table.
     *
     * @param db the database to fill
     */
    public void mockEvents(SQLiteDatabase db) {
        for (MockEvents notification : MockEvents.values()) {
            for (String s : notification.getStatements()) {
                db.execSQL(s);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Returns a list of all events present in the database.
     *
     * @return a list of all Cap Sophia events, ordered by date
     */
    public List<EventSophia> getAllEvents() {
        List<EventSophia> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SELECT_NOTIF_BASE + " ORDER BY " + EVENT_DATE + " DESC;", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            result.add(new EventSophia(cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getLong(4), requestTags(cursor, db)));
            cursor.moveToNext();
        }
        cursor.close();
        return result;
    }

    /**
     * Returns a list of strings associated to a specific event.
     *
     * @param event the cursor placed on the concerned event
     * @param db    the database containing the tags
     * @return a list of all tags tied to the event the cursor points to
     */
    private List<String> requestTags(Cursor event, SQLiteDatabase db) {
        List<String> tags = new ArrayList<>();
        Cursor tagsCursor = db.rawQuery("SELECT * FROM " + TAG_TO_NOTIFICATION
                + " INNER JOIN " + TAGS_TABLE + " ON " + TAG_TO_NOTIFICATION + "." + TagEntry._ID + " = "
                + TAGS_TABLE + "." + TagEntry._ID + " WHERE "
                + EventEntry._ID + " = ?;", new String[]{Integer.toString(event.getInt(0))});
        tagsCursor.moveToFirst();
        while (!tagsCursor.isAfterLast()) {
            tags.add(tagsCursor.getString(3));
            tagsCursor.moveToNext();
        }
        tagsCursor.close();
        return tags;
    }

    /**
     * Returns a list containing all of the available tags.
     *
     * @return a list of all event tags
     */
    public List<String> getAllTags() {
        List<String> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TAGS_TABLE + ";", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            result.add(cursor.getString(1));
            cursor.moveToNext();
        }
        cursor.close();
        return result;
    }

    /**
     * Returns a list of all events matching the specified parameters.
     *
     * @param offset  the maximum offset relative to the base date allowed for the event
     *                (if 0, the event won't be filtered by offset)
     * @param include the list of tags to whitelist (no whitelisting done if empty)
     * @param exclude the list of tags to blacklist (no blacklisting done if empty)
     * @return a list containing all the filtered events, ordered by date
     */
    public List<EventSophia> filterEvents(long offset, List<String> include, List<String> exclude) {
        List<EventSophia> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = SELECT_NOTIF_BASE;
        if (offset != 0) {
            query += " WHERE " + EVENT_DATE + ">" + Long.toString((BASE_DATE - offset) * 1000L);
        }
        Cursor request = db.rawQuery(query + " ORDER BY " + EVENT_DATE + " DESC", null);
        request.moveToFirst();
        for (; !request.isAfterLast(); request.moveToNext()) {
            List<String> tags = requestTags(request, db);
            if (!include.isEmpty() && !matchesTag(tags, include)) {
                continue;
            }
            if (!exclude.isEmpty() && matchesTag(tags, exclude)) {
                continue;
            }
            result.add(new EventSophia(request.getString(1), request.getString(2),
                    request.getString(3), request.getLong(4), tags));
        }
        request.close();
        return result;
    }

    /**
     * Checks if an event matches the specified tag list.
     *
     * @param event   the list of tags of the event
     * @param matches the list of tags to match against
     * @return true if one of the tags matches, false otherwise
     */
    public boolean matchesTag(List<String> event, List<String> matches) {
        for (String match : matches) {
            if (event.contains(match)) {
                return true;
            }
        }
        return false;
    }

}
