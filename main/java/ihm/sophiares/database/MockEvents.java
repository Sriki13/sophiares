package ihm.sophiares.database;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static ihm.sophiares.database.DBStatements.ASSOCIATION_BASE;
import static ihm.sophiares.database.DBStatements.DAY_S;
import static ihm.sophiares.database.DBStatements.EVENT_INSERT_BASE;
import static ihm.sophiares.database.DBStatements.MONTH_S;
import static ihm.sophiares.database.MockTag.CLOSING;
import static ihm.sophiares.database.MockTag.EVENT_TAG;
import static ihm.sophiares.database.MockTag.FLASH_SALE;
import static ihm.sophiares.database.MockTag.GAME;
import static ihm.sophiares.database.MockTag.MOTHERS_DAY;
import static ihm.sophiares.database.MockTag.NEWS;
import static ihm.sophiares.database.MockTag.OPENING;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

/**
 * Enum for all the fake events. Stores the INSERT statements to use to add them to the database.
 */
public enum MockEvents {

    EVENT_0(0, asList(GAME, MOTHERS_DAY),
            "JEU PHOTOS POUR LA FÊTE DES MÈRES !", "Cap Sophia",
            "Le samedi 27 mai un jeu photos est organisé en place centrale, où vous aurez la chance de pouvoir gagner 100 euros en bons d'achat !\n" +
                    "De 10h à 13h et de 14h à 18h, chaque maman pourra venir accompagnée d'un de ses enfants et sera prise en photo avec pour fond " +
                    "le photocall de la Galerie, en place centrale. \nSur notre page Facebook, un album sera dédié à cette occasion. Les 5 duo " +
                    "mère/enfant qui auront leur photo la plus likée gagneront 100 euros en bon d'achat ! \n" +
                    "De plus, notre jury choisira 5 autres duo mère/enfant qui l'aura le plus touché.",
            convertDate(27, 5)),

    EVENT_1(1, singletonList(OPENING),
            "OUVERTURE EXCEPTIONNELLE JEUDI 25 MAI", "Cap Sophia",
            "La Galerie Cap Sophia sera ouverte le jeudi de l'Ascension, le 25 mai.",
            convertDate(24, 5)),

    EVENT_2(2, singletonList(OPENING),
            "OUVERTURE BOUTIQUE KAPORAL", "Kaporal",
            "Vendredi 19 mai, la boutique KAPORAL Jeans ouvre ses portes à quelques mètres de la place centrale, entre Depech'Mod et Place au gout.",
            convertDate(19, 5)),

    EVENT_3(3, singletonList(EVENT_TAG),
            "MÉDECINS DU MONDE - VENEZ LES RENCONTRER", "Cap Sophia",
            "Les équipes de Médecins du monde seront dans votre galerie Cap Sophia !",
            convertDate(30, 5)),

    EVENT_4(4, asList(FLASH_SALE, MOTHERS_DAY),
            "SEPHORA ET LA FÊTE DES MÈRES !", "Sephora",
            "Du 22 au 27 mai le tapis rose vous est déroulé dans votre boutique Sephora !\nPlusieurs animations aux programmes telles que des " +
                    "prestations de maquillage flash offert, 10€ offert pour 100€ d’achat etc.",
            convertDate(22, 5)),

    EVENT_5(5, singletonList(OPENING),
            "INAUGURATION NOUVEL ESPACE CHAUSSURE", "Cap-Sophia",
            "Le 27 mai prochain, Cap Sophia Nice Masséna ouvriront un tout nouvel Espace Chaussure. Un cadre d’exception de 500m2, le plus vaste du" +
                    " centre-ville de Nice, entièrement repensé en termes d’agencement, d’offre et de services.",
            convertDate(27, 5)),

    EVENT_6(6, singletonList(EVENT_TAG),
            "NOUVEAU PROGRAMME FIDELITE", "Cap-Sophia",
            "Cap Sophia lancera son nouvea programme de fidélité le 20 mai prochain",
            convertDate(20, 5)),

    EVENT_7(7, singletonList(CLOSING),
            "FEMETURE DE THEORY & PHILIPP LIM", "Cap-Sophia",
            "Cap-Sophia est ravi de vous annoncer l’ouverture prochaine de la boutique éphémère PHILLIP LIM & THEORY.",
            convertDate(5, 5)),

    EVENT_8(8, singletonList(FLASH_SALE),
            "-50% SUR LE 2ÈME ARTICLE ACHETÉ* !", "Izac",
            "En ce moment chez Izac: Profitez de 50% de remise sur le deuxième article acheté sur les vestes casual, chemises, pulls et t-shirts de" +
                    " la collection été 2017 dans votre boutique Izac !",
            convertDate(18, 5)),

    EVENT_9(9, singletonList(EVENT_TAG),
            "MUSIQUE JAZZ ET BLUES", "Cap-Sophia",
            "Une animation musicale qui se déroulera mardi 16 mai de 14h à 19h.",
            convertDate(16, 5)),

    EVENT_10(10, singletonList(GAME),
            "GRAND JEU LE VOTE GAGNANT : TENTEZ DE REMPORTER LE CADEAU DE VOTRE CHOIX", "Cap-Sophia",
            "Du 19 avril au 31 mai, participez à notre grand jeu « Le Vote Gagnant » et tentez de remporter le cadeau de votre choix! A gagner : " +
                    "une TV, un smartphone, des montres connectées, un robot de cuisine, un drone, des cartes cadeaux et bien plus encore!",
            convertDate(19, 4)),

    EVENT_11(11, singletonList(NEWS),
            "BLABLACAR", "Cap-Sophia",
            "Nouveau ! La Galerie soutient le covoiturage en partenariat avec BlaBlaCar : des places de parking sont dédiées aux covoitureurs.",
            convertDate(24, 5));


    private List<String> statements;

    /**
     * Constructor
     *
     * @param id     the id of the event
     * @param tags   the list of tags of the event
     * @param values in that order:
     *               the title of the event,
     *               its author,
     *               its summary,
     *               a unix timestamp in ms as String.
     */
    MockEvents(int id, List<MockTag> tags, String... values) {
        statements = new ArrayList<>();
        statements.add(buildBaseInsert(id, values));
        for (MockTag tag : tags) {
            statements.add(buildAssociationInsert(id, tag));
        }
    }

    /**
     * Utility method to create a date relative to a 2017 base.
     *
     * @param day   the day of the date
     * @param month the month of the date
     * @return a String representation of a long of the intended 2017 date in ms
     */
    private static String convertDate(int day, int month) {
        return Long.toString(new Date((1483228800L + MONTH_S * (month - 1) + DAY_S * (day - 1)) * 1000L).getTime());
    }

    /**
     * Builds the main INSERT of the event (in the event table).
     *
     * @param id     the id of the event
     * @param values the String values of the rest of the columns, in the correct order
     * @return a String containing the SQL insert statement corresponding to this event
     */
    private String buildBaseInsert(int id, String... values) {
        StringBuilder result = new StringBuilder(EVENT_INSERT_BASE);
        result.append(id).append(",");
        for (String value : values) {
            value = value.replaceAll("'", "''");
            result.append("'").append(value).append("',");
        }
        result.deleteCharAt(result.length() - 1);
        result.append(");");
        return result.toString();
    }

    /**
     * Builds the INSERT associating this event to the event tags in the tag association table.
     *
     * @param id  the id of the event
     * @param tag one of the tags of this event
     * @return a String containing the SQL insert statement linking this event to its tag
     */
    private String buildAssociationInsert(int id, MockTag tag) {
        return ASSOCIATION_BASE + tag.getId() + "," + id + ");";
    }

    public List<String> getStatements() {
        return statements;
    }

}
