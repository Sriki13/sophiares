package ihm.sophiares.database;

import android.provider.BaseColumns;

import static ihm.sophiares.database.DBStatements.EventEntry.EVENT_AUTHOR;
import static ihm.sophiares.database.DBStatements.EventEntry.EVENT_DATE;
import static ihm.sophiares.database.DBStatements.EventEntry.EVENT_DETAILS;
import static ihm.sophiares.database.DBStatements.EventEntry.EVENT_TABLE;
import static ihm.sophiares.database.DBStatements.EventEntry.EVENT_TITLE;
import static ihm.sophiares.database.DBStatements.TagEntry.TAGS_TABLE;
import static ihm.sophiares.database.DBStatements.TagEntry.TAG_NAME;

/**
 * Class used to store database table names and columns,
 * along with some utilities related to dates.
 *
 * @author Guillaume André
 */
public class DBStatements {

    //Associations tables
    public static final String TAG_TO_NOTIFICATION = "tag_to_notification";

    //Inserts
    public static final String EVENT_INSERT_BASE = "INSERT INTO " + EVENT_TABLE +
            "(" + EventEntry._ID + "," + EVENT_TITLE + "," + EVENT_AUTHOR +
            "," + EVENT_DETAILS + "," + EVENT_DATE + ") VALUES (";

    public static final String TAGS_INSERT_BASE = "INSERT INTO " + TAGS_TABLE +
            "(" + TagEntry._ID + "," + TAG_NAME + ") VALUES (";

    public static final String ASSOCIATION_BASE = "INSERT INTO " + TAG_TO_NOTIFICATION +
            "(" + TagEntry._ID + "," + EventEntry._ID + ") VALUES (";

    //Utilities
    public static final long MINUTE_S = 60L;
    public static final long HOUR_S = MINUTE_S * 60L;
    public static final long DAY_S = HOUR_S * 24L;
    public static final long WEEK_S = DAY_S * 7L;
    public static final long MONTH_S = DAY_S * 30L;
    public static final long YEAR_S = MONTH_S * 12L;
    public static final long BASE_DATE = 1494956984L + MONTH_S;

    /**
     * Here to avoid instantiation.
     */
    private DBStatements() {
    }

    //Tables
    public static class EventEntry implements BaseColumns {
        public static final String EVENT_TABLE = "events";
        public static final String EVENT_TITLE = "title";
        public static final String EVENT_AUTHOR = "author";
        public static final String EVENT_DETAILS = "details";
        public static final String EVENT_DATE = "date";
        public static final String _ID = "notif_id";
    }

    public static class TagEntry implements BaseColumns {
        public static final String TAGS_TABLE = "tags";
        public static final String TAG_NAME = "tag_name";
        public static final String _ID = "tag_id";
    }

}
