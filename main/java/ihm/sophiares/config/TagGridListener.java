package ihm.sophiares.config;

/**
 * Interface used to detect when a TagGridFragment is done inflating
 * and is available for further actions.
 *
 * @author Guillaume André
 */
public interface TagGridListener {

    /**
     * Intended to be called right after onCreateView()
     */
    void notifyLayoutDrawn();

}
