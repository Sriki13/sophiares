package ihm.sophiares.config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A configuration file handler for the tags filtering the notifications.
 * The information is stored in JSON format as  a file named "config" in the
 * root of the app file directory.
 *
 * @author Guillaume André
 */
public class Config {

    private List<String> include;
    private List<String> exclude;

    /**
     * Constructor. Used to create a new config file.
     *
     * @param include the event tag whitelist
     * @param exclude the event tag blacklist
     */
    public Config(List<String> include, List<String> exclude) {
        this.include = include;
        this.exclude = exclude;
    }

    /**
     * Constructor. Used to load a pre-existing config file.
     *
     * @param config the file containing the config
     * @throws IOException   if failing to read the file
     * @throws JSONException if the JSON format of the config is incorrect
     */
    public Config(File config) throws IOException, JSONException {
        BufferedReader br = new BufferedReader(new FileReader(config));
        String line;
        StringBuilder content = new StringBuilder();
        while ((line = br.readLine()) != null) {
            content.append(line);
        }
        JSONObject json = new JSONObject(content.toString());
        include = new ArrayList<>();
        exclude = new ArrayList<>();
        JSONArray includeArray = json.getJSONArray("whitelist");
        JSONArray excludeArray = json.getJSONArray("blacklist");
        for (int i = 0; i < includeArray.length(); i++) {
            include.add(includeArray.getString(i));
        }
        for (int i = 0; i < excludeArray.length(); i++) {
            exclude.add(excludeArray.getString(i));
        }
    }

    public List<String> getInclude() {
        return include;
    }

    public List<String> getExclude() {
        return exclude;
    }

    /**
     * Saves this tag configuration, erasing any pre-existing configuration.
     *
     * @param directory the app main file directory
     * @throws JSONException if failing to create the JSON
     * @throws IOException   if failing to write the file
     */
    public void save(File directory) throws JSONException, IOException {
        JSONArray includeArray = new JSONArray();
        for (String s : include) {
            includeArray.put(s);
        }
        JSONArray excludeArray = new JSONArray();
        for (String s : exclude) {
            excludeArray.put(s);
        }
        JSONObject json = new JSONObject();
        json.put("whitelist", includeArray);
        json.put("blacklist", excludeArray);
        File file = new File(directory, "config");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(json.toString());
        bw.close();
    }

}
