package ihm.sophiares.config;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;

import ihm.sophiares.R;
import ihm.sophiares.tag.TagGridsFragment;

/**
 * Fragment used to manage the configuration of the notifications received.
 *
 * @author Guillaume André
 */
public class ConfigFragment extends Fragment implements TagGridListener {

    private static int RECEIVE_PREFERENCE = 0;
    private static int DISMISS_PREFERENCE = 1;

    private static String RECEIVE_KEY = "receive_notifications";

    private TagGridsFragment tags;
    private boolean tagsLoaded;
    private View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        tagsLoaded = false;
        root = inflater.inflate(R.layout.config_main, container, false);
        RadioButton positive = (RadioButton) root.findViewById(R.id.receive_notification);
        positive.setOnClickListener((x) -> initGrids());
        RadioButton negative = (RadioButton) root.findViewById(R.id.dismiss_notification);
        negative.setOnClickListener((x) -> unloadTagFragment());
        root.findViewById(R.id.cancel_config).setOnClickListener((x) -> cancelConfig());
        root.findViewById(R.id.save_config).setOnClickListener((x) -> saveConfig());
        loadPreferences();
        return root;
    }

    /**
     * Loads user preferences.
     */
    private void loadPreferences() {
        SharedPreferences preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        int radioMode = preferences.getInt(RECEIVE_KEY, RECEIVE_PREFERENCE);
        RadioGroup group = (RadioGroup) root.findViewById(R.id.radio_group_config);
        if (radioMode == RECEIVE_PREFERENCE) {
            initGrids();
            group.check(R.id.receive_notification);
        } else {
            unloadTagFragment();
            group.check(R.id.dismiss_notification);
        }
    }

    /**
     * Inflates the grids containing the tags.
     */
    private void initGrids() {
        tags = new TagGridsFragment();
        tags.setListener(this);
        getFragmentManager().beginTransaction()
                .add(R.id.grid_frame, tags)
                .commit();
    }

    /**
     * Here to avoid a NPE when attempting to load the tag configuration before the grids
     * are actually loaded. This will be called by the grids once done inflating.
     */
    @Override
    public void notifyLayoutDrawn() {
        tagsLoaded = true;
        loadConfig();
    }

    /**
     * Hides the tag grids.
     */
    private void unloadTagFragment() {
        tagsLoaded = false;
        if (tags != null) {
            getFragmentManager().beginTransaction()
                    .hide(tags)
                    .commit();
        }
        tags = null;
    }

    /**
     * Attempts to load the pre-existing configuration of tags filters into the grids.
     */
    public void loadConfig() {
        try {
            File input = new File(getActivity().getFilesDir(), "config");
            if (input.exists()) {
                Config config = new Config(input);
                tags.loadIntoGrids(config.getInclude(), config.getExclude());
            }
        } catch (IOException e) {
            Log.e("CONFIG", "Could not load config file:");
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e("CONFIG", "Incorrect format for the config file");
        }
    }

    /**
     * Cancels the changes done to the configuration.
     */
    public void cancelConfig() {
        loadPreferences();
    }

    /**
     * Saves the changes done to the configuration, overwriting the previous one.
     */
    public void saveConfig() {
        if (tagsLoaded) {
            Config config = new Config(tags.fetchTags(true), tags.fetchTags(false));
            try {
                config.save(getActivity().getFilesDir());
            } catch (JSONException | IOException e) {
                Log.e("CONFIG", "Failed to save config file");
                e.printStackTrace();
            }
        } else {
            File before = new File(getActivity().getFilesDir(), "config");
            if (!before.delete()) {
                Log.e("CONFIG", "Failed to delete previous configuration");
            }
        }
        SharedPreferences preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(RECEIVE_KEY, tagsLoaded ? RECEIVE_PREFERENCE : DISMISS_PREFERENCE);
        editor.apply();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Configuration enregistrée.");
        builder.setPositiveButton("OK", (dialog, which) -> {
        });
        builder.create().show();
    }

}
