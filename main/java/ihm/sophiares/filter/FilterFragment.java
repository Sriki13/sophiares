package ihm.sophiares.filter;

import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import ihm.sophiares.MainActivity;
import ihm.sophiares.R;
import ihm.sophiares.database.SophiaDB;
import ihm.sophiares.event.EventFragment;
import ihm.sophiares.tag.TagGridsFragment;

/**
 * Fragment used to create a new filter for events.
 *
 * @author Guillaume André
 */
public class FilterFragment extends Fragment {

    private EditText nameField;
    private TagGridsFragment grids;
    private Spinner spinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.new_filter, container, false);
        nameField = (EditText) root.findViewById(R.id.nameField);
        root.findViewById(R.id.confirm_and_save_filter).setOnClickListener((x) -> confirm(true));
        root.findViewById(R.id.confirm_filter).setOnClickListener((x) -> confirm(false));
        spinner = (Spinner) root.findViewById(R.id.date_spinner);
        ArrayAdapter<CharSequence> dateAdapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.date_choices, android.R.layout.simple_spinner_item);
        dateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dateAdapter);
        grids = new TagGridsFragment();
        getFragmentManager().beginTransaction()
                .add(R.id.grids_wrapper, grids)
                .commit();
        return root;
    }

    /**
     * Filters the events according to the changes made on screen.
     *
     * @param save true to also save the filter to the device memory (in which case a name is necessary),
     *             false otherwise
     */
    private void confirm(boolean save) {
        if (save && nameField.getText().toString().equals("")) {
            displayError();
            return;
        }
        EventFragment fragment = new EventFragment();
        SophiaDB db = SophiaDB.getInstance(getActivity().getApplicationContext());
        List<String> whitelist = grids.fetchTags(true);
        List<String> blacklist = grids.fetchTags(false);
        fragment.setContent(db.filterEvents(DateChoice.getOffset(spinner.getSelectedItem().toString()),
                whitelist, blacklist));
        getFragmentManager().beginTransaction()
                .replace(R.id.main_frame, fragment)
                .addToBackStack(null)
                .commit();
        if (save) {
            saveFilter(whitelist, blacklist);
        }
    }

    /**
     * Displays an error dialog if attempting to save a filter without naming it.
     */
    private void displayError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Un filtre doit être nommé pour pouvoir être enregistré");
        builder.setPositiveButton("OK", (dialog, which) -> {
        });
        builder.create().show();
    }

    /**
     * Saves a filter configuration in the app files folder.
     *
     * @param whitelist the list of whitelisted tags (empty if none)
     * @param blacklist the list of blacklisted tags (empty if none)
     */
    private void saveFilter(List<String> whitelist, List<String> blacklist) {
        String name = nameField.getText().toString();
        Filter filter = new Filter(getActivity().getApplicationContext(),
                DateChoice.getOffset(spinner.getSelectedItem().toString()), whitelist, blacklist, name);
        filter.save();
        ((MainActivity) getActivity()).addFilterToMenu(name);
    }

}
