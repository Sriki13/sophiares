package ihm.sophiares.filter;

import static ihm.sophiares.database.DBStatements.DAY_S;
import static ihm.sophiares.database.DBStatements.MONTH_S;
import static ihm.sophiares.database.DBStatements.WEEK_S;
import static ihm.sophiares.database.DBStatements.YEAR_S;

/**
 * Enum for the date choices available in the filter dropdown menu.
 * Effectively a glorified lookup map to link the String values to
 * their long time counterpart.
 * <p>
 * Only works as long as the enum values are the same as the one
 * contained in the resource file contained in the values resource
 * folder.
 *
 * @author Guillaume André
 */
public enum DateChoice {

    ALL("Tous les évènements", 0),
    TWO_LAST_DAYS("Les 2 derniers jours", DAY_S * 2),
    LAST_WEEK("La dernière semaine", WEEK_S),
    LAST_TWO_WEEKS("Les deux dernières semaines", WEEK_S * 2),
    LAST_MONTH("Le dernier mois", MONTH_S),
    LAST_3_MONTHS("Les 3 derniers mois", MONTH_S * 3),
    LAST_YEAR("La dernière année", YEAR_S);

    private String title;
    private long offset;

    /**
     * Constructor.
     *
     * @param title  the title of the choice
     * @param offset the long date offset corresponding to the choice
     */
    DateChoice(String title, long offset) {
        this.title = title;
        this.offset = offset;
    }

    /**
     * Returns the offset date offset corresponding to one of the choices
     * in the dropdown menu.
     *
     * @param title the choice selected
     * @return the date corresponding to the choice, or 0 if not found
     */
    public static long getOffset(String title) {
        for (DateChoice d : values()) {
            if (d.getTitle().equals(title)) {
                return d.getOffset();
            }
        }
        return 0;
    }

    private String getTitle() {
        return title;
    }

    private long getOffset() {
        return offset;
    }


}
