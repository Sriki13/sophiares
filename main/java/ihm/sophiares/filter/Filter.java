package ihm.sophiares.filter;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A filter used to select a subset of events to display.
 *
 * @author Guillaume André
 */
public class Filter {

    private long offset;
    private List<String> includeTags;
    private List<String> excludeTags;
    private String name;
    private Context context;

    /**
     * Constructor.
     *
     * @param context     the context of the application
     * @param offset      the date offset to match against (0 if none)
     * @param includeTags the tag whitelist of the filter (empty if none)
     * @param excludeTags the tag blacklist of the filter (empty if none)
     * @param name        the name of the filter
     */
    public Filter(Context context, long offset, List<String> includeTags, List<String> excludeTags, String name) {
        this.offset = offset;
        this.includeTags = includeTags;
        this.excludeTags = excludeTags;
        this.name = name;
        this.context = context;
    }

    /**
     * Constructor. Used to retrieve an existing filter from the device
     *
     * @param json the json representation of the filter
     */
    public Filter(JSONObject json) {
        try {
            name = json.getString("name");
            offset = json.getLong("offset");
            JSONArray include = json.getJSONArray("whitelist");
            includeTags = new ArrayList<>();
            for (int i = 0; i < include.length(); i++) {
                includeTags.add(include.getString(i));
            }
            JSONArray exclude = json.getJSONArray("blacklist");
            excludeTags = new ArrayList<>();
            for (int i = 0; i < exclude.length(); i++) {
                excludeTags.add(exclude.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public long getOffset() {
        return offset;
    }

    public List<String> getIncludeTags() {
        return includeTags;
    }

    public List<String> getExcludeTags() {
        return excludeTags;
    }

    /**
     * Saves the filter to the device memory, as a JSON file in the app files folder.
     */
    public void save() {
        try {
            JSONObject json = new JSONObject();
            json.put("name", name);
            json.put("offset", offset);
            JSONArray include = new JSONArray();
            for (String tag : includeTags) {
                include.put(tag);
            }
            json.put("whitelist", include);
            JSONArray exclude = new JSONArray();
            for (String tag : excludeTags) {
                exclude.put(tag);
            }
            json.put("blacklist", exclude);
            File file = new File(context.getFilesDir(), name + ".json");
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(json.toString());
            bw.close();
            System.err.println("saving file to" + name + ".json");
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

}
