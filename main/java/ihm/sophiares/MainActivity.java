package ihm.sophiares;

import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import ihm.sophiares.config.ConfigFragment;
import ihm.sophiares.contact.ContactFragment;
import ihm.sophiares.database.SophiaDB;
import ihm.sophiares.delete.DeletePickerFragment;
import ihm.sophiares.event.EventFragment;
import ihm.sophiares.filter.Filter;
import ihm.sophiares.filter.FilterFragment;

/**
 * Main (and only) activity of the app.
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static List<Integer> DEFAULT_MENU =
            Arrays.asList(R.id.nav_add_filter, R.id.nav_all_events, R.id.nav_delete_filter);

    private SophiaDB db;
    private Menu parent;

    /**
     * Utility method to remove the ".json" extension from a file name.
     *
     * @param original the original file name
     * @return the same file name, without the ".json" extension
     */
    public static String removeJSON(String original) {
        return original.substring(0, original.length() - 5);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = SophiaDB.getInstance(getApplicationContext());
        setContentView(R.layout.main_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu main = navigationView.getMenu();
        MenuItem notif = main.getItem(0);
        parent = notif.getSubMenu();
        navigationView.setNavigationItemSelectedListener(this);
        EventFragment fragment = new EventFragment();
        fragment.setContent(db.getAllEvents());
        getFragmentManager().beginTransaction()
                .add(R.id.main_frame, fragment)
                .commit();
        loadMenuFilters();
    }

    /**
     * Loads a fragment in the main content frame.
     *
     * @param fragment the fragment to display
     */
    private void loadContent(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.main_frame, fragment)
                .addToBackStack(null)
                .commit();
    }

    /**
     * Adds the filters saved to the device memory in the menu.
     */
    public void loadMenuFilters() {
        clearMenuItems();
        File directory = getApplicationContext().getFilesDir();
        for (File file : directory.listFiles()) {
            if (file.getName().endsWith(".json")) {
                addFilterToMenu(removeJSON(file.getName()));
            }
        }
    }

    /**
     * Clears the menu of all filters items.
     */
    private void clearMenuItems() {
        for (int i = 0; i < parent.size(); i++) {
            MenuItem item = parent.getItem(i);
            if (!isDefaultButton(item.getItemId())) {
                parent.removeItem(item.getItemId());
            }
        }
    }

    /**
     * Checks if an item menu id is one of the default buttons (not a filter).
     *
     * @param item the id to check
     * @return true if the id is one of the default buttons, false otherwise
     */
    private boolean isDefaultButton(int item) {
        for (int id : DEFAULT_MENU) {
            if (item == id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a filter menu item.
     *
     * @param name the name of the filter
     */
    public void addFilterToMenu(String name) {
        MenuItem item = parent.add(Menu.NONE, Menu.NONE, 50, name);
        item.setIcon(R.drawable.menu_arrow);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_all_events:
                EventFragment all = new EventFragment();
                all.setContent(db.getAllEvents());
                loadContent(all);
                break;
            case R.id.nav_add_filter:
                loadContent(new FilterFragment());
                break;
            case R.id.nav_call:
                loadContent(new ContactFragment());
                break;
            case R.id.nav_select:
                loadContent(new ConfigFragment());
                break;
            case R.id.nav_delete_filter:
                DialogFragment delete = new DeletePickerFragment();
                delete.show(getFragmentManager(), "");
            default:
                loadFilter(item.getTitle().toString());
                break;
        }
        return true;
    }

    /**
     * Loads a filter and displays the matching events.
     *
     * @param name the name of the filter
     */
    private void loadFilter(String name) {
        try {
            File file = new File(getFilesDir(), name + ".json");
            System.err.println("attempting to open" + name + ".json");
            BufferedReader br = new BufferedReader(new FileReader(file));
            StringBuilder content = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                content.append(line);
            }
            br.close();
            Filter filter = new Filter(new JSONObject(content.toString()));
            EventFragment fragment = new EventFragment();
            fragment.setContent(db.filterEvents(filter.getOffset(), filter.getIncludeTags(),
                    filter.getExcludeTags()));
            loadContent(fragment);
        } catch (IOException | JSONException e) {
            Log.d("EXCEPTION", "Could not load or retrieve filter " + name);
        }
    }

}
