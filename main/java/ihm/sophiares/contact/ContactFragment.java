package ihm.sophiares.contact;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import ihm.sophiares.R;

/**
 * Fragment controlling the contact screen.
 *
 * @author Guillaume André
 */
public class ContactFragment extends Fragment implements ContactPickerListener {

    private View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.contact_main, container, false);
        ImageButton phone = (ImageButton) root.findViewById(R.id.callButton);
        phone.setOnClickListener((x) -> doPhoneCall());
        Button sms = (Button) root.findViewById(R.id.sendSms);
        sms.setOnClickListener((x) -> sendSms());
        LinearLayout base = (LinearLayout) root.findViewById(R.id.contact_base);
        base.setOnClickListener((x) -> chooseContact());
        base.setMinimumHeight(100);
        MockContact defaultContact = MockContact.getDefault();
        switchContact(defaultContact.getPosition(), defaultContact.getName(), defaultContact.getPhone());
        return root;
    }

    /**
     * Calls the currently selected contact.
     */
    public void doPhoneCall() {
        TextView phone = (TextView) root.findViewById(R.id.number);
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + removeSpaces(phone.getText().toString())));
        startActivity(intent);
    }

    /**
     * Utility method to remove cosmetic spaces in the phone number String.
     *
     * @param original the String to format
     * @return the same string, without its spaces
     */
    private String removeSpaces(String original) {
        return original.replaceAll(" ", "");
    }

    /**
     * Sends an sms containing the text in the field to the currently selected contact.
     * Displays a confirmation dialog if successful.
     */
    public void sendSms() {
        EditText message = (EditText) root.findViewById(R.id.smsText);
        TextView phone = (TextView) root.findViewById(R.id.number);
        if (message.getText().toString().equals("")) {
            return;
        }
        SmsManager sm = SmsManager.getDefault();
        sm.sendTextMessage(removeSpaces(phone.getText().toString()), null, message.getText().toString(), null, null);
        Log.d("SMS", "sent:" + message.getText().toString());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage("Message envoyé.")
                .setPositiveButton("OK", (dialog, which) -> {
                });
        builder.create().show();
    }

    /**
     * Opens a dialog to select a contact.
     */
    public void chooseContact() {
        ContactPickerFragment fragment = new ContactPickerFragment();
        fragment.init(this);
        getFragmentManager().beginTransaction()
                .add(fragment, null)
                .commit();
    }

    /**
     * Switches the currently selected contact to a new one.
     *
     * @param position the position of the contact
     * @param name     the name of the contact
     * @param phone    the contact phone number
     */
    @Override
    public void switchContact(String position, String name, String phone) {
        TextView positionText = (TextView) root.findViewById(R.id.position);
        positionText.setText(position);
        TextView nameText = (TextView) root.findViewById(R.id.contactName);
        nameText.setText(name);
        TextView phoneText = (TextView) root.findViewById(R.id.number);
        phoneText.setText(phone);
    }

}
