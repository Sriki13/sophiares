package ihm.sophiares.contact;

/**
 * Enum for all the fake contacts.
 *
 * @author Guillaume André
 */
public enum MockContact {

    RES_CENTRE("Responsable du centre", "Jean-Michel", "06 89 28 18 27"),
    RES_2BE("Manager 2BOr2Have", "Jean-Eudes Tequila", "21 00 00 00 00"),
    RES_3("Poste santé", "Barry Covillon","66 66 66 66 66");

    private String position;
    private String name;
    private String phone;

    /**
     * Constructor.
     *
     * @param position the position of the contact
     * @param name     the name of the contact
     * @param phone    the phone number of the contact
     */
    MockContact(String position, String name, String phone) {
        this.position = position;
        this.name = name;
        this.phone = phone;
    }

    /**
     * Returns the contact to be displayed by default.
     *
     * @return the default contact
     */
    public static MockContact getDefault() {
        return RES_CENTRE;
    }

    public String getPosition() {
        return position;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

}
