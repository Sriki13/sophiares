package ihm.sophiares.contact;

import android.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ihm.sophiares.R;

/**
 * Adapter for the different fake contacts in the MockContact enum.
 *
 * @author Guillaume André
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder> {

    private ContactPickerListener listener;
    private DialogFragment dialog;

    /**
     * Constructor.
     *
     * @param listener the listener to call when the user chooses the contact
     * @param dialog the dialog using this adapter
     */
    public ContactAdapter(ContactPickerListener listener, DialogFragment dialog) {
        this.listener = listener;
        this.dialog = dialog;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflated = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact, parent, false);
        TextView position = (TextView) inflated.findViewById(R.id.position);
        TextView name = (TextView) inflated.findViewById(R.id.contactName);
        TextView number = (TextView) inflated.findViewById(R.id.number);
        inflated.setOnClickListener((x) -> {
            listener.switchContact(position.getText().toString(), name.getText().toString(), number.getText().toString());
            dialog.dismiss();
        });
        return new ContactViewHolder(inflated, position, name, number);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        holder.bind(MockContact.values()[position]);
    }

    @Override
    public int getItemCount() {
        return MockContact.values().length;
    }
}
