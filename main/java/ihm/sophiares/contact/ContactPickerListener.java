package ihm.sophiares.contact;

/**
 * Interface used to select a contact with the ContactPickerFragment dialog.
 *
 * @author Guillaume André
 */
public interface ContactPickerListener {

    /**
     * Switches the currently selected contact to a new one.
     *
     * @param position the position of the selected contact
     * @param name     the name of the selected contact
     * @param phone    the phone number of the selected contact
     */
    void switchContact(String position, String name, String phone);

}
