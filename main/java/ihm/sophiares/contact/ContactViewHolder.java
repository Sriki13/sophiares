package ihm.sophiares.contact;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * View holder for a single contact.
 *
 * @author Guillaume André
 */
public class ContactViewHolder extends RecyclerView.ViewHolder {

    private TextView position;
    private TextView name;
    private TextView number;

    /**
     * Constructor.
     *
     * @param itemView the contact main view
     * @param position the text view containing the position of the contact
     * @param name     the text view containing the name of the contact
     * @param number   the text view containing the name of the contact
     */
    public ContactViewHolder(View itemView, TextView position, TextView name, TextView number) {
        super(itemView);
        this.position = position;
        this.name = name;
        this.number = number;
    }

    /**
     * Binds the contact view to a fake contact.
     *
     * @param contact the contact to bind this item view to
     */
    public void bind(MockContact contact) {
        position.setText(contact.getPosition());
        name.setText(contact.getName());
        number.setText(contact.getPhone());
    }

}
