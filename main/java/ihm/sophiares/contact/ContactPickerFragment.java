package ihm.sophiares.contact;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import ihm.sophiares.R;

/**
 * Fragment for the contact picker dialog.
 *
 * @author Guillaume André
 */
public class ContactPickerFragment extends DialogFragment {

    private ContactPickerListener listener;

    /**
     * Sets the listener to call once a new contact is selected.
     *
     * @param listener the listener to this dialog
     */
    public void init(ContactPickerListener listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View root = inflater.inflate(R.layout.contact_picker, null);
        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.contactRecycler);
        ContactAdapter adapter = new ContactAdapter(listener, this);
        RecyclerView.LayoutManager manager = new GridLayoutManager(getActivity().getApplicationContext(), 2);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        builder.setView(root);
        return builder.create();
    }

}
